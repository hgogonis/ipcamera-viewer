// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
angular.module('myApp', ['ionic'])

.config(function($stateProvider, $urlRouterProvider, $httpProvider) {
    $stateProvider
    .state('all', {
            url: '/all',
            templateUrl: 'all.html',
            controller: 'AllCameraController as AllCtrl',
            cache: false
    })
    .state('camera', {
        url: 'camera/:cam',
        templateUrl: 'single.html',
        controller: 'SingleCameraController as CamCtrl',
        cache: false,
        resolve: {
            cam: function($stateParams, CameraService) {
                return CameraService.getCamera($stateParams.cam)
            }
        }
     })
    .state('settings', {
        url: '/settings',
        templateUrl: 'settings.html',
        controller: 'SettingsController as SettingsCtrl'
    })
    .state('login', {
        url: '/login',
        templateUrl: 'login.html',
        controller: 'LoginController'
    });

    $urlRouterProvider.otherwise("/all");
    $httpProvider.interceptors.push('authInterceptor');
})

.run(function($ionicPlatform, $window, CameraService) {
    $ionicPlatform.ready(function() {
        // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
        // for form inputs)
        if(window.cordova && window.cordova.plugins.Keyboard) {
            cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
        }
        if(window.StatusBar) {
            StatusBar.styleDefault();
        }
        if(wipeOnLoad) {
            $window.localStorage.clear();
        }
     });
})

.factory('authInterceptor', function($rootScope, $q, $cookies, $location) {
    return {
      request: function(config) {
        config.headers = config.headers || {};
        if ($cookies['token']) {
          config.headers.Authorization = 'Bearer ' + $cookies['token']);
        }
        return config;
      },
      responseError: function(response) {
        if (response.status === 401) {
          $location.path('/login');
          $cookies.remove('token');
        }
        return $q.reject(response);
      }
    };
  })
